package ru.t1.chubarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model) throws AbstractException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void clear();

    @NotNull
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull  String id) throws AbstractException;

    @Nullable
    M findOneByIndex(@NotNull  Integer index) throws AbstractException;

    @NotNull
    M remove(@NotNull M model) throws AbstractException;

    @Nullable
    M removeOneById(@NotNull String id) throws AbstractException;

    @Nullable
    M removeOneByIndex(@NotNull Integer index) throws AbstractException;

    void removeAll();

    int getSize();

    boolean existsById(@NotNull String id);

}
