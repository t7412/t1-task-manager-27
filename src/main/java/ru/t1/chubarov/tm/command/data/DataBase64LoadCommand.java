package ru.t1.chubarov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.Domain;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";
    @NotNull
    private final String DESCRIPTION = "Load data from base64 file.";

    @SneakyThrows
    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD BASE64]");
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE_64));
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable byte[] bytes = new BASE64Decoder().decodeBuffer(base64Data);
        @NotNull ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
